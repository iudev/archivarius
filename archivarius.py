import sys, os, zipfile, datetime
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5.QtCore import QTimer


class Example(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi('archivarius.ui', self)
        self.timerb = QTimer()
        self.DATA_START.clicked.connect(self.start)
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Information)
        self.msg.setText("Timer is started.")
        self.msg.setInformativeText("After specified time backups will be created.")
        self.msg.setWindowTitle("Timer Start")
        self.msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        self.notify = QMessageBox()
        self.notify.setIcon(QMessageBox.Information)
        self.notify.setText("Backup is done.")
        self.notify.setInformativeText("Your backup is ready.")
        self.notify.setWindowTitle("Ready")
        self.notify.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        self.timerb.timeout.connect(self.archive)

    def start(self):
        temptime = self.DATA_TimeForBackup.time()
        secs = temptime.second()
        mins = temptime.minute()
        hrs = temptime.hour()
        total = (mins * 60) + secs + (hrs * 60 * 60)
        self.timerb.setInterval(int(total) * 1000)
        self.timerb.start()
        print(total)
        self.msg.setDetailedText("Delaying for " + str(total) + " seconds, after that we will do an archivation.")
        self.msg.exec_()

    def zipdir(self, path, ziph):
        for root, dirs, files in os.walk(path):
            print(root, dirs, files)
            for file in files:
                fileName = os.path.join(root, file)
                arcName = fileName.replace(path, '')
                ziph.write(fileName, arcname=arcName)

    def archive(self):
        now = datetime.datetime.now()
        print('Get now')
        fileName = self.DATA_PathToBackUps.text() + str(now.year) + '-' + str(now.month) + '-' + str(
            now.day) + '_' + str(now.hour) + '-' + str(now.minute) + '-' + str(now.second) + '.zip'
        print(fileName)
        try:
            zipf = zipfile.ZipFile(fileName, mode='w', compression=zipfile.ZIP_DEFLATED)
            print('zipf created')
            fname = self.DATA_BackupFolder.text()
            print(fname)
            self.zipdir(fname, zipf)
            print('zipdir called')
            zipf.close()
            print('zipf called')
            if self.DATA_Notify.isChecked():
                self.notify.exec_()
        except Exception as e:
            print(e)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
